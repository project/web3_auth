<?php

namespace Drupal\web3_auth\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;

/**
 * Block provides a 'Connect Wallet' button.
 *
 * If user is logged in then block displays the user's wallet address.
 *
 * @Block(
 *   id = "web3_auth_block",
 *   admin_label = @Translation("Web3 Auth Block"),
 * )
 */
class Web3AuthBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $renderable = [
      '#theme' => 'web3_auth_template',
      // '#test_var' => 'test variable',
    ];

    return $renderable;
  }


}
