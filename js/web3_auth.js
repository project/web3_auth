(function (Drupal, drupalSettings) {
  "use strict";


// ========================================================
/**
 * Helper function that converts hex values to strings
 * @param {*} hex 
 * @returns 
 */
const hex2ascii = (hex) => {
    console.group('hex2ascii');
    console.log({ hex });
    let str = '';
    for (let i = 0; i < hex.length; i += 2) {
        const v = parseInt(hex.substr(i, 2), 16);
        if (v) str += String.fromCharCode(v);
    }
    console.groupEnd();
    return str;
};

/**
 * When the chainId has changed
 * @param {string|null} chainId
 */
const onChainChanged = (chainId) => {
    console.group('onChainChanged');
    console.log({ chainId });

    // Get the UI element that displays the wallet network
    const preWalletNetwork = document.getElementById('pre-wallet-network');

    if (!chainId) {
        CHAIN_CONNECTED.name = null;
        CHAIN_CONNECTED.id = null;

        // Set the network to blank
        preWalletNetwork.innerHTML = ``;
    } else {
        const parsedChainId = parseInt(`${chainId}`, 16);
        CHAIN_CONNECTED.name = CHAINS_DICTIONARY?.[parsedChainId]['name'];
        CHAIN_CONNECTED.id = parsedChainId;

        const buttonNetwork = document.getElementById('button-network');
        const buttonAddNetwork = document.getElementById('button-add-network');
        const divErrorNetwork = document.getElementById('div-error-network');
        // Experimental - future release with contract interactions
        // const formContractReadButton = document.querySelector('#form-contract-read button');
        // const formContractWriteInput = document.querySelector('#form-contract-write input');
        // const formContractWriteButton = document.querySelector('#form-contract-write button');

        if (parsedChainId !== CHAIN_ID_REQUIRED) {
            // Show error elements

            // debug
            console.warn('parsedChainId: ' + parsedChainId + ' != CHAIN_ID_REQUIRED: ' + CHAIN_ID_REQUIRED);

            buttonNetwork.classList = `${buttonNetwork.classList.value.replaceAll('hidden', '')}`;
            buttonAddNetwork.classList = `${buttonAddNetwork.classList.value.replaceAll('hidden', '')}`;

            divErrorNetwork.classList = `${divErrorNetwork.classList.value.replaceAll('hidden', '')}`;
            divErrorNetwork.children[1].innerHTML = `${CHAIN_CONNECTED.name}`;
            // Experimental - future release with contract interactions
            // Disable forms
            // formContractReadButton.setAttribute('disabled', true);
            // formContractWriteInput.setAttribute('disabled', true);
            // formContractWriteButton.setAttribute('disabled', true);
        } else {

            // debug
            console.warn('parsedChainId: ' + parsedChainId + ' == CHAIN_ID_REQUIRED: ' + CHAIN_ID_REQUIRED);

            // Hide error elements
            buttonNetwork.classList = `${buttonNetwork.classList.value} hidden`;
            buttonAddNetwork.classList = `${buttonNetwork.classList.value} hidden`;
            divErrorNetwork.classList = `${divErrorNetwork.classList.value} hidden`;
            divErrorNetwork.children[1].innerHTML = '';
            // Experimental - future release with contract interactions
            // Enable forms
            // formContractReadButton.removeAttribute('disabled');
            // formContractWriteInput.removeAttribute('disabled');
            // formContractWriteButton.removeAttribute('disabled');
        }

        // Set the network to show the current connected network
        preWalletNetwork.innerHTML = `${CHAIN_CONNECTED?.id} / ${CHAIN_CONNECTED?.name}`;
    }

    console.log({ CHAIN_CONNECTED });
    console.groupEnd();
};

/**
 * When wallet connects or disconnects with accountsChanged event
 * @param {*} accounts Array of accounts that have changed - typicall array of one
 */
const onAccountsChanged = async (accounts) => {
    console.group('onAccountsChanged');
    console.log({ accounts });


    if (drupalSettings.web3_auth.web3_connect_only != 1 && drupalSettings.web3_auth.logged_in != false) {
        console.warn('Web3 wallet changed !! Drupal account management will not handle this!!');
    }

    // No accounts found - use onWalletDisconnect to update UI
    if (accounts.length === 0) {
        onChainChanged(null);
        onWalletDisconnect();
    } else {
        // Accounts found - use callback for onWalletConnection to update UI
        WALLET_CONNECTED = accounts?.[0];

        // Update chain connected
        const chainId = await ethereum.request({ method: 'eth_chainId' });

        onChainChanged(chainId);
        onWalletConnection();


            // log out Drupal
            try {
              const drupalLogout = await logoutDrupal();
              }
  
          catch (error) {
           console.log({ error });
              // If error connecting, display the error message
              devErrorConnect.innerHTML = error?.message ?? 'Unknown wallet connection error.'
              devErrorConnect.classList = devErrorConnect.classList.value.replaceAll('hidden', '');
          }
    }

    console.groupEnd();
};

/**
 * When wallet disconnect occurs
 */
 const onWalletDisconnect = () => {
    console.group('onWalletDisconnect');

    // Hide connected section
    const sectionConnected = document.getElementById('section-connected');
    sectionConnected.classList = 'hidden';

    // Enabled connect button
    const buttonConnect = document.getElementById('button-connect');
    buttonConnect.removeAttribute('disabled');
    buttonConnect.innerHTML = 'Connect Wallet';

    console.groupEnd();
};

/**
 * When a wallet connection occurs
 */
 const onWalletConnection = () => {
    console.group('onWalletConnection');

    // Disable connect button
    const buttonConnect = document.getElementById('button-connect');
    buttonConnect.setAttribute('disabled', true);
    buttonConnect.innerHTML = 'Connected';

    // Show connected section
    const sectionConnected = document.getElementById('section-connected');
    sectionConnected.classList = '';

    // Set the wallet address to show the user
    const preWalletAddress = document.getElementById('pre-wallet-address');
    preWalletAddress.innerHTML = WALLET_CONNECTED;

    console.groupEnd();
 };

/**
 * When Connect Button is clicked
 */
const connect = async () => {
    console.group('connect');

    // Reset our error element each time the button is clicked
    const devErrorConnect = document.getElementById('div-error-connect');
    devErrorConnect.innerHTML = '';
    devErrorConnect.classList = devErrorConnect.classList.value.includes('hidden')
        ? devErrorConnect.classList.value
        : `${devErrorConnect.classList.value} hidden`;

    try {
        const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
        WALLET_CONNECTED = accounts[0];

        // Update chain connected
        const chainId = await ethereum.request({ method: 'eth_chainId' });
        onChainChanged(chainId);
        
        // Update wallet connection preference to true
        localStorage.setItem(WALLET_CONNECTION_PREF_KEY, true);

        onWalletConnection();

        // Drupal log in if required
        if (drupalSettings.web3_auth.web3_connect_only != 1 && drupalSettings.web3_auth.logged_in != true) {
            console.warn('Drupal login with web 3 required.');

            // signAndLoginDrupal
            try {
                const drupalLogin = await signAndLoginDrupal(WALLET_CONNECTED);
                }
    
            catch (error) {
             console.log({ error });
                // If error connecting, display the error message
                devErrorConnect.innerHTML = error?.message ?? 'Unknown wallet connection error.'
                devErrorConnect.classList = devErrorConnect.classList.value.replaceAll('hidden', '');
            }
        }


    } catch (error) {
        console.log({ error });
        // If error connecting, display the error message
        devErrorConnect.innerHTML = error?.message ?? 'Unknown wallet connection error.'
        devErrorConnect.classList = devErrorConnect.classList.value.replaceAll('hidden', '');
    }

    console.groupEnd();
};

/**
 * When Disconnect button is clicked
 */
 const disconnect = async () => {
    console.group('disconnect');

    WALLET_CONNECTED = false;
    
    onChainChanged(null);

    // Remove wallet connection preference
    localStorage.removeItem(WALLET_CONNECTION_PREF_KEY);

    onWalletDisconnect();
           
    // Drupal log in if required
    if (drupalSettings.web3_auth.web3_connect_only != 1 && drupalSettings.web3_auth.logged_in != false) {
           console.warn('Drupal logout with web 3 required.');

            // log out Drupal
            try {
                const drupalLogout = await logoutDrupal();
                }
    
            catch (error) {
             console.log({ error });
                // If error connecting, display the error message
                devErrorConnect.innerHTML = error?.message ?? 'Unknown wallet connection error.'
                devErrorConnect.classList = devErrorConnect.classList.value.replaceAll('hidden', '');
            }
    }
    console.groupEnd();
};

/**
 * Switches network to CHAIN_ID_REQUIRED
 */
const switchNetwork = async () => {
    console.group('switchNetwork');
    console.log({ CHAIN_ID_REQUIRED: CHAIN_ID_REQUIRED.toString(16) });
    try {
        await window.ethereum.request({ method: 'wallet_switchEthereumChain', params: [{ chainId: `0x${CHAIN_ID_REQUIRED.toString(16)}` }], })
    } catch (error) {
        console.log({ error });
    }
    console.groupEnd();
};

/**
 * Add network CHAIN_ID_REQUIRED
 */
const addNetwork = async () => {
    console.group('addNetwork');
    console.log({ CHAIN_ID_REQUIRED: CHAIN_ID_REQUIRED.toString(16) });
    try {
        await window.ethereum.request({ method: 'wallet_addEthereumChain', 
        params: [{
            chainId: `0x${CHAIN_ID_REQUIRED.toString(16)}`,
            rpcUrls: [CHAINS_DICTIONARY[CHAIN_ID_REQUIRED]['rpcUrl']],
            chainName: `${CHAINS_DICTIONARY[CHAIN_ID_REQUIRED]['name']}`,
            nativeCurrency: {
              name: `${CHAINS_DICTIONARY[CHAIN_ID_REQUIRED]['symbol']}`,
              symbol: `${CHAINS_DICTIONARY[CHAIN_ID_REQUIRED]['symbol']}`,
              decimals: 18
            },
            blockExplorerUrls: [CHAINS_DICTIONARY[CHAIN_ID_REQUIRED]['expUrl']]
            }], 
        })
    } catch (error) {
        console.log({ error });
    }
    console.groupEnd();
};


  // Constants
// ========================================================
/**
* To keep track of which wallet is connected throughout our app
*/
let WALLET_CONNECTED = '';

/**
* localStorage key
*/
let WALLET_CONNECTION_PREF_KEY = 'WC_PREF';

/**
* Current chain connected with chain id and name as a object { id: 1, name: "Ethereum Mainnet" }
* 
* These should be moved in module admin to allow user customization?
* 
*/
const CHAIN_CONNECTED = {
id: null,
name: null
};



/**
* Chain ids and infos
*/
const CHAINS_DICTIONARY = eval("(" + drupalSettings.web3_auth.web3_chains_dictionnary + ")");


/**
* Chain ids required
*/
const CHAIN_ID_REQUIRED = Number(drupalSettings.web3_auth.web3_chain_id_required);


/**
* Experimental - future release with contract interactions 
* Contracts dictionnary
*/
const CONTRACT_ON_CHAINS = eval("(" + drupalSettings.web3_auth.web3_contracts_dictionnary + ")");
console.log(CONTRACT_ON_CHAINS);

/**
* Experimental - future release with contract interactions 
* Contract ABI
*/
const CONTRACT_ABI = eval("(" + drupalSettings.web3_auth.web3_contract_abi + ")");
console.log(CONTRACT_ABI);



  // Initial Script Loaded On Window Loaded
  // ========================================================
  /**
   * Init
   */
  async function initialize() {
    console.group("window.onload");

    // Replace elements with required chain name
    const chainNameReplace = document.querySelectorAll(".chain-name");
    chainNameReplace.forEach((el) => {
      el.innerHTML = `${CHAINS_DICTIONARY[CHAIN_ID_REQUIRED]['name']}`;
    });

    // Replace elements with required chain name and link
    // const chainLinkReplace = document.querySelectorAll(".chain-link");
    // chainLinkReplace.forEach((el) => {
    //   el.innerHTML = `${CHAINS_DICTIONARY[CHAIN_ID_REQUIRED]['name']}`;
    //   el.setAttribute(
    //     "href",
    //     `${CHAINS_DICTIONARY[CHAIN_ID_REQUIRED]['expUrl']}/address/${CONTRACT_ON_CHAINS[CHAIN_ID_REQUIRED]}`
    //   );
    // });

    // All HTML Elements
    const buttonConnect = document.getElementById("button-connect");
    const buttonDisconnect = document.getElementById("button-disconnect");
    const buttonNetwork = document.getElementById("button-network");
    const buttonAddNetwork = document.getElementById("button-add-network");
    // Experimental - future release with contract interactions
    // const formContractRead = document.getElementById("form-contract-read");
    // const formContractWrite = document.getElementById("form-contract-write");

    // Event Interactions
    buttonConnect.addEventListener("click", connect);
    buttonDisconnect.addEventListener("click", disconnect);
    buttonNetwork.addEventListener("click", switchNetwork);
    buttonAddNetwork.addEventListener("click", addNetwork);
    // Experimental - future release with contract interactions
    // formContractRead.addEventListener("submit", onSubmitContractRead);
    // formContractWrite.addEventListener("submit", onSubmitContractWrite);



    // Check if browser has wallet integration
    if (typeof window?.ethereum !== "undefined") {
      // Enable Button
      buttonConnect.removeAttribute("disabled");
      buttonConnect.innerHTML = "Connect Wallet";

      // Events
      window.ethereum.on("accountsChanged", onAccountsChanged);
      window.ethereum.on("chainChanged", onChainChanged);

      // Check if already connected with the number of permissions we have
      const hasWalletPermissions = await window.ethereum.request({
        method: "wallet_getPermissions",
      });
      console.log({ hasWalletPermissions });

      // Retrieve wallet connection preference from localStorage
      const shouldBeConnected =
        JSON.parse(localStorage.getItem(WALLET_CONNECTION_PREF_KEY)) || false;
      console.log({ shouldBeConnected });

      // If wallet has permissions update the site UI
      if (hasWalletPermissions.length > 0 && shouldBeConnected) {
        // Retrieve chain
        const chainId = await ethereum.request({ method: "eth_chainId" });
        onChainChanged(chainId);
        connect();
      }
    }

    console.groupEnd();
  }






  // Call /Web3/login with params adr & signature
  // ========================================================
  /**
   *  Drupal login with Signature
   */

 async function signAndLoginDrupal(signerAddress) {

    var xhr = new XMLHttpRequest();
    xhr.open("GET", Drupal.url("web3/signature"), false);
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    xhr.send();
    
    if (xhr.status === 200) {
      var data = JSON.parse(xhr.responseText);
      var msg2sign = data[0];
      console.log("Message to sign: ", msg2sign);

      try {
        // const signerAddress = await web3ethers.getSigner().getAddress();
        const signature = await window.ethereum.request({
          method: "personal_sign",
          params: [msg2sign, signerAddress],
        });
        // console.log(signature);
        const xhr = new XMLHttpRequest();
        xhr.open("POST", Drupal.url("web3/login"), false);
        xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
  
        const requestData = {
          address: signerAddress,
          signature: signature,
        };
  
        console.log(
          "Login request data : " +
            requestData["address"] +
            " - " +
            requestData["signature"]
        );
  
        xhr.send(JSON.stringify(requestData));
  
        if (xhr.status === 200) {
          const data = JSON.parse(xhr.responseText);
          console.log("Drupal Login: ", data);
          window.location.reload(false);
        } else {
          console.error("Error during Drupal login:", xhr.status, xhr.statusText);
        }
      } catch (error) {
        console.error("Error during login:", error);
      }
    
    }
 }


  // Call /Web3/logout => logout from Drupal
  // ========================================================
  /**
   *  Drupal logout
   */
  async function logoutDrupal() {

    console.warn('Drupal logout with web 3 required.');
    var xhr = new XMLHttpRequest();
    xhr.open("GET", Drupal.url("web3/logout"), false);
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    xhr.send();
    
    if (xhr.status === 200) {
      console.log("Disconnected from app.");
      window.location.reload(true);
    } else {
        console.error("Error during Drupal logout:", xhr.status, xhr.statusText);
      }
    
  }
  

 // Drupal behavior for Web3 Auth block
 // ========================================================
  /**
   *  Drupal behavior
   */
  Drupal.behaviors.Web3AuthBehavior = {
    attach: function (context, settings) {


        
      once("Web3AuthBehavior", "#web3-auth-block", context).forEach(function () {

        // Check user log status
        console.log('user logged in : ' + drupalSettings.web3_auth.logged_in);

        // Initialize
        console.log("initialize...");
        initialize();
      });
    },
  };
})(Drupal, drupalSettings);
