<?php

namespace Drupal\web3_auth\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure API Pléiade LemonLDAP fields settings.
 */
class Web3AuthConfig extends ConfigFormBase {


  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'web3_auth_config_form';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'web3_auth.settings'
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('web3_auth.settings');

    $form['web3_chains_dictionnary'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Web3 module chains dictionnary'),
      '#default_value' => $config->get('web3_chains_dictionnary'),
      '#description' => $this->t('Chains dictionnary with RPC urls, token names, explorers url.'),
    ];
 
    $form['web3_chain_id_required'] = [
      '#type' => 'number',
      '#title' => $this->t('Web3 module chain id required'),
      '#default_value' => $config->get('web3_chain_id_required'),
      '#description' => $this->t('Chain id required for the module.'),
    ];

    $form['web3_connect_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Web3 wallet connect only (no user management/log in in Drupal)'),
      '#default_value' => $config->get('web3_connect_only'),
      '#description' => $this->t('Check this box to keep normal login behavior and connect wallet in frontend only.'),
    ];


    // For future realease -- smart contracts interaction
    $form['web3_contract'] = [
      '#type' => 'details',
      '#title' => $this->t('[EXPERIMENTAL] Web3 smart contract info'),
      '#open' => FALSE,
      '#tree' => TRUE,
    ];
    $form['web3_contract']['web3_contracts_dictionnary'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Web3 module contracts dictionnary'),
      '#default_value' => $config->get('web3_contracts_dictionnary'),
      '#description' => $this->t('Contracts dictionnary with chanId & contract adresses pairs.'),
    ];
    $form['web3_contract']['web3_contract_abi'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Web3 module contract ABI'),
      '#default_value' => $config->get('web3_contract_abi'),
      '#description' => $this->t('Contract ABI.'),
    ];

    return parent::buildForm($form, $form_state);
  }
 
  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
      $this->config('web3_auth.settings')

      // Set the submitted configuration setting.
      ->set('web3_chains_dictionnary', $form_state->getValue('web3_chains_dictionnary'))
      ->set('web3_chain_dictionnary', $form_state->getValue('web3_chain_dictionnary'))
      ->set('web3_chain_id_required', $form_state->getValue('web3_chain_id_required'))
      ->set('web3_chain_explorers', $form_state->getValue('web3_chain_explorers'))
      ->set('web3_chain_tokens', $form_state->getValue('web3_chain_tokens'))
      ->set('web3_connect_only', $form_state->getValue('web3_connect_only')) 
      ->set('web3_contract', $form_state->getValue('web3_contract')) 
      ->set('web3_contracts_dictionnary', $form_state->getValue('web3_contract.web3_contract_dictionnary')) 
      ->set('web3_contract_abi', $form_state->getValue('web3_contract.web3_contract_abi')) 
      ->save();

    parent::submitForm($form, $form_state);
  }

}